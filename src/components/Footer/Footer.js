import React from 'react';
import './Footer.scss';

function Footer() {
    return (
        <div className="footer-container">
            <ul className="footer-lists">
                <li>
                    About
                </li>
                <li>
                    Contact Us
                </li>
            </ul>
        </div>
    )
}

export default Footer;