import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Product from './components/product/Products';
import Navigation from './components/Navigation/Navigation';
import TabsDetails from './components/Tabs/Tabs';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';
import { Tabs } from 'react-bootstrap';
import Footer from './components/Footer/Footer'

ReactDOM.render(
  <React.StrictMode>
    <Navigation />
    <TabsDetails />
    <Product />
    <Footer />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.unregister();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
